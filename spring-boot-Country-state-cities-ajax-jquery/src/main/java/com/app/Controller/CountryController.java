package com.app.Controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Collector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.entities.Cities;
import com.app.entities.Countries;
import com.app.entities.States;
import com.app.repo.citiesRepo;
import com.app.repo.countrysRepo;
import com.app.repo.stateRepo;

@Controller
public class CountryController {

//	@Autowired
//	CountriesService countriesService;
	@Autowired
	// outside use nhin krych ahe so private
	private countrysRepo countrysRepo;

	@Autowired
	private stateRepo stateRepo;

	@Autowired
	private citiesRepo citiesRepo;

	@GetMapping(value = "/")
	public String view() {
		return "index";
	}

	@ModelAttribute("countries")
	public Map<Integer, String> getCountry() {
		List<Countries> list = countrysRepo.findAll();
		Map<Integer, String> map = list.stream().collect(Collectors.toMap(Countries::getId, Countries::getName));
		return map;

	}


	@PostMapping(value = "loadStates")
	@ResponseBody
	public List<States> loadStates(@RequestParam Integer countryId) {
		System.out.println(countryId);
		List<States> states = stateRepo.findAllStatesByCountryId(countryId);
		return states;
	}

	@PostMapping(value = "loadCities")
	@ResponseBody
	public List<Cities> loadCities(@RequestParam Integer stateId) {
		List<Cities> cities = citiesRepo.findAllCitiesByStateId(stateId);
		return cities;
	}
}
