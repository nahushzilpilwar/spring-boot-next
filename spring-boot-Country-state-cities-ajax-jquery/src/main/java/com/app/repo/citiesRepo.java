package com.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.Cities;

public interface citiesRepo extends JpaRepository<Cities, Integer> {

	@Query("from Cities where states.id=:stateId")
	public List<Cities> findAllCitiesByStateId(Integer stateId); 
		

}
