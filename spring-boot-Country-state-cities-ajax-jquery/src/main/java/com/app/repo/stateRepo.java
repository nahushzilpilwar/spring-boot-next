package com.app.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.States;

public interface stateRepo extends JpaRepository<States, Integer> {

	@Query("from States where countries.id=:countryId")
	public List<States> findAllStatesByCountryId(Integer countryId); 
		


}
